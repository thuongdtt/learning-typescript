
class Ulti {
    static timUocChung(a: number, b: number): number {
        let aRutGon = Math.abs(a);
        let bRutGon = Math.abs(b);
        while (aRutGon != bRutGon) {
            if (aRutGon > bRutGon) {
                aRutGon -= bRutGon;
            } else {
                bRutGon -= aRutGon
            }
        }
        return aRutGon
    }
}

class PhanSo {
    //field
    public tuSo: number;
    public mauSo: number;

    //constructor
    constructor(tuSo: number, mauSo: number) {
        if (mauSo === 0) {
            throw new Error("Mau so khong the bang 0");
        } else {
            this.tuSo = tuSo;
            this.mauSo = mauSo;
        }
    }

    //method
    cong(phanso: PhanSo): PhanSo {
        let mauSoCong = this.mauSo * phanso.mauSo;
        let tuSoCong = this.tuSo * phanso.mauSo + phanso.tuSo * this.mauSo;
        let ketQuaCong = new PhanSo(tuSoCong, mauSoCong);
        return ketQuaCong.rutGon();
    }
    tru(phanso: PhanSo): PhanSo {
        let mauSoTru = this.mauSo * phanso.mauSo;
        let tuSoTru = this.tuSo * phanso.mauSo - phanso.tuSo * this.mauSo;
        let ketQuaTru = new PhanSo(tuSoTru, mauSoTru);
        return ketQuaTru.rutGon();
    }
    nhan(phanso: PhanSo): PhanSo {
        let mauSoNhan = this.mauSo * phanso.mauSo;
        let tuSoNhan = this.tuSo * phanso.tuSo;
        let ketQuaNhan = new PhanSo(tuSoNhan, mauSoNhan);
        return ketQuaNhan.rutGon()
    }
    chia(phanso: PhanSo): PhanSo {
        let mauSoChia = this.mauSo * phanso.tuSo;
        let tuSoChia = this.tuSo * phanso.mauSo;
        let ketQuaChia = new PhanSo(tuSoChia, mauSoChia);
        return ketQuaChia.rutGon()
    }

    rutGon(): PhanSo {
        let uocChung = Ulti.timUocChung(this.tuSo, this.mauSo);
        return new PhanSo(this.tuSo / uocChung, this.mauSo / uocChung);
    }
    toString():string {
        return this.tuSo + "/" + this.mauSo
    } 
}
try {
    var phanSo1 = new PhanSo(-4, 3);
    var phanSo2 = new PhanSo(1, 2);
}
catch (err) {
    console.log(err);
}
let cong = phanSo1.cong(phanSo2);
let tru = phanSo1.tru(phanSo2);
let nhan = phanSo1.nhan(phanSo2);
let chia = phanSo1.chia(phanSo2);
console.log("Cong: " + cong);
console.log("Tru: " + tru);
console.log("Nhan: " + nhan);
console.log("Chia: " + chia);
console.log("Done");
