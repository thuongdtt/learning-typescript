class Program {
    static main() {

    }
}

class Board {
    // Fields
    public soHang: number;
    public soCot: number;

    private _data: number[]; //[0, 2, 4, 6, 1, 3, 5, 7];

    constructor(row: number, col: number) {
        this.soHang = row;
        this.soCot = col;
        this._data = [];
    }

    store(pos: Position): void {
        this._data[pos.col] = pos.row;
    }

    // Methods
    isValid(pos: Position): Boolean {
        // check col
        if (this._data[pos.col] != undefined) {
            return false;
        }

        // check row
        for (let i = 0; i < this._data.length; i++) {
            if (this._data[i] === pos.row) {
                return false;
            }
        }

        // 
        for (let i = pos.col, j = pos.row; i < this.soCot && j < this.soHang; i++ , j++) {
            if (this._data[i] === j) {
                return false;
            }
        }
        for (let i = pos.col - 1, j = pos.row - 1; i > 0 && j > 0; i-- , j--) {
            if (this._data[i] === j) {
                return false;
            }
        }

        return true;
    }
}

class Position {
    public row: number;
    public col: number;
    constructor(row: number, col: number) {
        this.row = row;
        this.col = col;
    }
}

class Queen {

}



Program.main();