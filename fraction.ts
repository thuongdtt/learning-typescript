
function timUocChung(a:number, b:number):number{
        var a1 = Math.abs(a);
        var b1 = Math.abs(b);
        while ( a1 != b1){
            if (a1 > b1){
                a1 -= b1;
            }else {
                b1 -= a1;
            }
        }
        return a1
    }

class PhanSo {
    //field
    tuSo:number;
    mauSo:number;

    //constructor
    constructor(tuSo:number, mauSo:number) {
        if (mauSo === 0) {
            throw new Error("Mau so khong the bang 0");
        }else {
        this.tuSo = tuSo;
        this.mauSo = mauSo;
        }
    }

    //method
    cong(phanso:PhanSo):PhanSo {
        var mauSoMoi = this.mauSo * phanso.mauSo;
        var tuSoMoi = this.tuSo * phanso.mauSo + phanso.tuSo * this.mauSo;
        var result = new PhanSo(tuSoMoi, mauSoMoi);
        return result.rutGon();
    }
    tru(phanso:PhanSo):PhanSo {
        var mauSoMoi2 = this.mauSo * phanso.mauSo;
        var tuSoMoi2 = this.tuSo * phanso.mauSo - phanso.tuSo * this.mauSo;
        var result2 = new PhanSo(tuSoMoi2, mauSoMoi2);
        return result2.rutGon();
    }
    nhan(phanso:PhanSo):PhanSo {
        var mauSoMoi3 = this.mauSo * phanso.mauSo;
        var tuSoMoi3 = this.tuSo * phanso.tuSo
        return new PhanSo(tuSoMoi3, mauSoMoi3).rutGon()
    }
    chia(phanso:PhanSo):PhanSo {
        var mauSoMoi4 = this.mauSo * phanso.tuSo;
        var tuSoMoi4 = this.tuSo * phanso.mauSo;
        return new PhanSo(tuSoMoi4, mauSoMoi4).rutGon()
    }
    
    rutGon():PhanSo {
        var uocChung = timUocChung(this.tuSo, this.mauSo);
        return new PhanSo(this.tuSo/uocChung, this.mauSo/uocChung);
    }
}
try{
    var phanSo1 = new PhanSo(-4, 0);
    var phanSo2 = new PhanSo(1, 2);
    var cong = phanSo1.cong(phanSo2);
    var tru = phanSo1.tru(phanSo2);
    var nhan = phanSo1.nhan(phanSo2);
    var chia = phanSo1.chia(phanSo2);
    console.log(cong);
    console.log(tru);
    console.log(nhan);
    console.log(chia);
}
catch(e){
    console.log(e);
}

console.log("Done");
